# Changelog for oblog

---

# In 0.1.1.0

- Adapt to skalibs 2.11.0.0

- Adapt to oblibs 0.1.4.0

---

# In 0.1.0.0

- first commit
