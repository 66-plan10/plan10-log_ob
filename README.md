![GitLabl Build Status](https://framagit.org/Obarun/oblog/badges/master/pipeline.svg)

oblog is a powerful, specialized echo tool. This is the exact same program than [66-yeller](https://web.obarun.org/software/66-tools/66-yeller.html). This reimplementation avoids the dependency of the complete [66-tools](https://web.obarun.org/software/66-tools) set of tools.

Installation
------------

See the INSTALL.md file.

Documentation
-------------

Online [documentation](https://web.obarun.org/software/oblog/)

Contact information
-------------------

* Email:
  Eric Vidal `<eric@obarun.org>`

* Web site:
  https://web.obarun.org/

* XMPP Channel:
  obarun@xmpp.obarun.org

Supports the project
---------------------

Please consider to make [donation](https://web.obarun.org/index.php?id=18)
