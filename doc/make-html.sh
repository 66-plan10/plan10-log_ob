#!/bin/sh

html='oblog'

if [ ! -d doc/html ]; then
    mkdir -p -m 0755 doc/html
fi

for i in ${html};do
     lowdown -s doc/${i}.md -o doc/html/${i}.html
done
